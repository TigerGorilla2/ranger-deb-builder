# Build ranger (with sixel image preview) deb-package in a docker container

Ofcourse, instead of `docker` you can use `podman` like
> `alias docker='podman'`

## Build the image
`docker build -t ranger-deb-builder .`

## Building ranger
```sh
docker run \
    --mount type=bind,src=out-debs,target=/out-debs \
    --name ranger \
    localhost/ranger-deb-builder
```

To run the build again just start the container again
```sh
docker start --latest \
    --filter name=ranger
```
