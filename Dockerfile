# This is a comment
FROM debian:bookworm

ENV root /src
ENV build $root/build
ENV out_debs /out-debs
ENV logs $root/logs

ENV DEBEMAIL server@home

WORKDIR $root
COPY . $root
VOLUME $build

# This way we start with an empty directory
RUN mkdir $logs

RUN cp /etc/apt/sources.list.d/debian.sources /etc/apt/sources.list.d/debian-src.sources
RUN sed -i '/^Types: deb$/ s++&-src+' /etc/apt/sources.list.d/debian-src.sources

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
    && apt-get install -y git dpkg-dev devscripts \
    && apt-get build-dep -y ranger


# Now you can `git -C $root pull origin main` to update a running container
RUN git remote set-url origin https://gitlab.com/TigerGorilla2/ranger-deb-builder.git

# Default to doing nothing and just staying alive
CMD ./build.sh
