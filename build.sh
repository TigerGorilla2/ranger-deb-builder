#!/bin/bash -ex
# Build ranger from source, add sixel image preview, and put it in the locally
# hosted repo.

base_version="1.9.3"
patch="5.1"
version="$base_version"-"$patch"
ranger_build=$build/ranger-$version

# it does not exist OR is not directory OR is an empty directory
if [ -e $out_debs/binary-amd64/ranger-$version.deb ]; then
    exit 0
fi

if [ -d $ranger_build ]; then
    rm -rf $ranger_build
fi

# get source
mkdir $ranger_build && cd $ranger_build
apt-get source ranger

# apply patch
cd "ranger-$base_version"
curl 'https://github.com/3ap/ranger/commit/ef9ec1f0e0786e2935c233e4321684514c2c6553.patch' \
| patch -p1
# disable tests because they cause problems (probably shouldnt do this xD)
sed -i -e '/^test:/ c\test:' Makefile

# build package
dch --newversion "$version" --package ranger --urgency low "add sixel image preview"
dpkg-buildpackage --build=binary --unsigned-source --unsigned-changes --root-command=fakeroot

# copy to repo
cd ..
mkdir $out_debs/binary-amd64 || true
cp ranger_${version}_all.deb $out_debs/binary-amd64/ranger-$version.deb
